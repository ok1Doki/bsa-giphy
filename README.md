# BSA-GIPHY #

WebAPI application that allows you to generate and store personalized GIFs using the GIPHY API.

### TECHNOLOGIES ###

This application uses the following technologies:

* Spring Boot 2.3.1
* Java 11
* Gradle 6.4.1

### TODO ###
- [x] Initialize project
- [x] Configure connection to Giphy API
- [x] Download gif's and save them on disk
- [x] Get a list of all files without linking to keywords
- [x] Clear cache on disk
- [x] Add endpoint for getting cache from disk
- [x] Generating GIF for users
- [x] Get a list of all files from a user folder on disk
- [x] Add the ability to save gif generation information to the history.csv file
- [ ] Add In Memory Cache (partially implemented)
- [x] Add filter for validating requests headers
- [x] Update build.gradle (add actuator dependency)
- [x] Add logging of incoming requests
- [x] Add application profiles
- [ ] Add custom exceptions and global exception handling
- [ ] Write tests
- [ ] Fix bugs
- [ ] Refactor, refactor and refactor code
- [x] Add application profiles

