package com.okidoki.bsa_giphy.controller;

import com.okidoki.bsa_giphy.model.GifFile;
import com.okidoki.bsa_giphy.model.dto.QueryResponseObject;
import com.okidoki.bsa_giphy.service.DiskCacheService;
import com.okidoki.bsa_giphy.service.GiphyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Map;

@RestController
@Slf4j
public class CacheController {

    private final GiphyService giphyService;
    private final DiskCacheService diskCacheService;

    @Autowired
    public CacheController(GiphyService giphyService, DiskCacheService diskCacheService) {
        this.giphyService = giphyService;
        this.diskCacheService = diskCacheService;
    }

    @GetMapping(path = "/cache")
    public ResponseEntity<?> getAllFromCache(@RequestParam(required = false) String query) {
        log.info("GET /cache - getting all files links from disk cache by query '" + query + "'");
        if (query != null) {
            return ResponseEntity.ok(
                    new QueryResponseObject(query, diskCacheService.getFilesLinksByQuery(query))
            );
        } else {
            return ResponseEntity.ok(diskCacheService.getAllObjectsLinks());
        }
    }

    @GetMapping(path = "/gifs")
    public ResponseEntity<?> getAllFromCache() {
        log.info("GET /gifs - getting all files from disk cache");
        return ResponseEntity.ok(diskCacheService.getAllFilesLinks());
    }

    @PostMapping(path = "/cache/generate")
    public ResponseEntity<?> generateGif(
            @RequestBody Map<String, String> requestBody) {

        String query = requestBody.get("query");
        log.info("POST /cache/generate - generating GIF by query '" + query + "'");

        try {
            GifFile gif = giphyService.generateGiphy(query);
            diskCacheService.saveFileToDiskCache(gif, query);
            return ResponseEntity.ok(
                    new QueryResponseObject(
                            query,
                            diskCacheService.getFilesLinksByQuery(query)
                    )
            );
        } catch (IOException e) {
            log.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @DeleteMapping(path = "/cache")
    public ResponseEntity<?> clearCache() {
        log.info("DELETE /cache - clearing all disk cache");
        try {
            diskCacheService.clearCache();
            return ResponseEntity.ok().build();
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

}
