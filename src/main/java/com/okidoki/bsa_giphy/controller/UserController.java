package com.okidoki.bsa_giphy.controller;

import com.okidoki.bsa_giphy.model.GifFile;
import com.okidoki.bsa_giphy.model.UserHistoryRecord;
import com.okidoki.bsa_giphy.service.DiskCacheService;
import com.okidoki.bsa_giphy.service.GiphyService;
import com.okidoki.bsa_giphy.service.UserService;
import com.okidoki.bsa_giphy.util.CsvHelper;
import com.okidoki.bsa_giphy.util.InMemoryCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

@RestController
@Slf4j
public class UserController {

    private final DiskCacheService diskCacheService;
    private final GiphyService giphyService;
    private final UserService userService;
    private final CsvHelper csvWriter;
    private final InMemoryCache<String, Map<String, Set<Path>>> inMemoryCache;

    @Autowired
    public UserController(DiskCacheService diskCacheService,
                          GiphyService giphyService,
                          UserService userService,
                          CsvHelper csvWriter,
                          InMemoryCache<String, Map<String, Set<Path>>> inMemoryCache) {
        this.diskCacheService = diskCacheService;
        this.giphyService = giphyService;
        this.userService = userService;
        this.csvWriter = csvWriter;
        this.inMemoryCache = inMemoryCache;
    }

    @PostMapping("/user/{userId}/generate")
    public String generateGifForUser(@RequestBody Map<String, String> requestBody, @PathVariable String userId) throws IOException {
        log.info("POST /user/" + userId + "/generate - generating GIF for user by query");

        String query = requestBody.get("query");

        String filePathInDiskCache = null;
        String savedFilePath = null;

        if (!requestBody.containsKey("force")
                && !Boolean.parseBoolean(requestBody.get("force"))
                && !diskCacheService.getFilesLinksByQuery(query).isEmpty()
        ) {
            List<String> gifs = diskCacheService.getFilesLinksByQuery(query);
            filePathInDiskCache = gifs
                    .get(new Random().nextInt(gifs.size()))
                    .replace(new File(".").getCanonicalPath(), "");
            savedFilePath = userService.saveFileToUserFolder(userId, query, filePathInDiskCache);
        } else {
            GifFile gif = giphyService.generateGiphy(query);
            diskCacheService.saveFileToDiskCache(gif, query);
            savedFilePath = userService.saveFileToUserFolder(userId, query, gif.getUrl(), gif.getFileName());
        }

        csvWriter.writeUserGeneratingHistory(
                new UserHistoryRecord(
                        LocalDate.now().toString(),
                        query,
                        savedFilePath),
                userId
        );
        userService.saveImageToInMemoryCache(userId, query, savedFilePath);
        inMemoryCache.get(userId).forEach((k, v) -> System.out.println(k + " - " + v));
        return savedFilePath;
    }

    @GetMapping(path = "/user/{userId}/all")
    public ResponseEntity<?> getAllFromCache(@PathVariable String userId) {
        log.info("GET /user/" + userId + "/all - getting all user GIFs from disk cache");

        return ResponseEntity.ok(userService.getAllObjectsLinks(userId));
    }

    @GetMapping(path = "/user/{userId}/history")
    public ResponseEntity getUserHistory(@PathVariable String userId) {
        log.info("GET /user/" + userId + "/history - getting user generating history");
        try {
            return ResponseEntity.ok(userService.getUserHistory(userId));
        } catch (IOException e) {
            log.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

}

