package com.okidoki.bsa_giphy.service;

import com.okidoki.bsa_giphy.model.GifFile;
import com.okidoki.bsa_giphy.model.dto.QueryResponseObject;
import com.okidoki.bsa_giphy.model.giphy.GiphyData;
import com.okidoki.bsa_giphy.util.FileHelper;
import com.okidoki.bsa_giphy.model.dto.giphy.*;
import com.okidoki.bsa_giphy.util.InMemoryCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.URI;
import java.util.Collections;

@Service
public class GiphyService {

    @Value("${giphy.url}")
    private String GIPHY_URL;

    @Value("${giphy.url.random}")
    private String RANDOM_ENDPOINT;

    @Value("${giphy.api-key}")
    private String GIPHY_API_TOKEN;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private DiskCacheService diskCacheService;

    @Autowired
    private InMemoryCache inMemoryCache;

    @Autowired
    private UserService userService;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public GifFile generateGiphy(String query) throws IOException {
        GiphyData gif = findRandomGiphys(query, 1).getData();
        String gifUrl = String.format("https://i.giphy.com/media/%s/200.gif", gif.getId());
        String fileName = gif.getId() + ".gif";
        return new GifFile(gif, gifUrl, fileName);
    }

    private GiphyRandomResponse findRandomGiphys(String query, int count) {
        try {
            URI uri = UriComponentsBuilder
                    .fromHttpUrl(GIPHY_URL + RANDOM_ENDPOINT)
                    .queryParam("tag", query)
                    .queryParam("api_key", GIPHY_API_TOKEN)
                    .queryParam("limit", count)
                    .queryParam("rating", "pg")
                    .build(Collections.emptyMap());
            ResponseEntity<GiphyRandomResponse> response =
                    restTemplate.getForEntity(uri, GiphyRandomResponse.class);
            return response.getBody();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}