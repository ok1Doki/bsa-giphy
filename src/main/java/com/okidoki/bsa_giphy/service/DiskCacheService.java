package com.okidoki.bsa_giphy.service;

import com.okidoki.bsa_giphy.model.GifFile;
import com.okidoki.bsa_giphy.model.dto.QueryResponseObject;
import com.okidoki.bsa_giphy.util.FileHelper;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class DiskCacheService {

    @Autowired
    private GiphyService giphyService;

    @Value("${app.cache.location}")
    private String cacheLocation;

    public List<String> getFilesLinksByQuery(String query) {
        List<String> gifPaths = null;
        try {
            String canonicalPath = new File(".").getCanonicalPath();
            String queryDirectoryPath = canonicalPath + cacheLocation + query + "/";

            try (Stream<Path> paths = Files.walk(Paths.get(queryDirectoryPath))) {
                gifPaths = paths
                        .filter(Files::isRegularFile)
                        .map(Path::toString)
                        .collect(Collectors.toList());
            }

        } catch (IOException e) {
            //TODO create custom exception
            e.printStackTrace();
        }
        return gifPaths == null ? Collections.emptyList() : gifPaths;
    }

    public List<String> getAllFilesLinks() {
        List<String> gifPaths = null;
        try {
            String queryDirectoryPath =
                    new File(".").getCanonicalPath() + cacheLocation;

            try (Stream<Path> paths = Files.walk(Paths.get(queryDirectoryPath))) {
                gifPaths = paths
                        .filter(Files::isRegularFile)
                        .map(Path::toString)
                        .collect(Collectors.toList());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return gifPaths == null ? Collections.emptyList() : gifPaths;
    }

    public List<QueryResponseObject> getAllObjectsLinks() {
        List<QueryResponseObject> gifPaths = null;
        try {
            String canonicalPath = new File(".").getCanonicalPath();
            String queryDirectoryPath = canonicalPath + cacheLocation;

            try (Stream<Path> paths = Files.walk(Paths.get(queryDirectoryPath))) {
                gifPaths = paths
                        .filter(Files::isDirectory)
                        .filter(path -> !path.getFileName().toString().equals("cache"))
                        .map(path -> new QueryResponseObject(
                                path.getFileName().toString(),
                                getFilesLinksByQuery(path.getFileName().toString())
                        ))
                        .collect(Collectors.toList());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return gifPaths == null ? Collections.emptyList() : gifPaths;
    }

    public String saveFileToDiskCache(GifFile gif, String query) throws IOException {
        String directoryPath = new File(".").getCanonicalPath() + cacheLocation + query + "/";
        FileHelper.downloadFile(gif.getUrl(), gif.getFileName(), directoryPath);
        return directoryPath + gif.getFileName();
    }

    public void clearCache() throws IOException {
        String canonicalPath = new File(".").getCanonicalPath();
        String directoryPath = canonicalPath + cacheLocation;
        FileUtils.cleanDirectory(new File(directoryPath));

    }
}
