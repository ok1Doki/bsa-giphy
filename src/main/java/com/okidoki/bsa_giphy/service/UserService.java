package com.okidoki.bsa_giphy.service;

import com.okidoki.bsa_giphy.model.UserHistoryRecord;
import com.okidoki.bsa_giphy.model.dto.QueryResponseObject;
import com.okidoki.bsa_giphy.util.CollectionUtils;
import com.okidoki.bsa_giphy.util.CsvHelper;
import com.okidoki.bsa_giphy.util.FileHelper;
import com.okidoki.bsa_giphy.util.InMemoryCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class UserService {

    private final InMemoryCache<String, Map<String, Set<Path>>> inMemoryCache;
    private final CsvHelper csvHelper;

    @Value("${app.users.location}")
    private String usersDirectoryLocation;

    @Autowired
    public UserService(InMemoryCache<String, Map<String, Set<Path>>> inMemoryCache, CsvHelper csvHelper) {
        this.inMemoryCache = inMemoryCache;
        this.csvHelper = csvHelper;
    }

    public String saveFileToUserFolder(String userId, String query, String gifUrl, String fileName) throws IOException {
        String directoryPath = String.format(
                "%s%s%s/%s/",
                new File(".").getCanonicalPath(),
                usersDirectoryLocation,
                userId,
                query
        );
        FileHelper.downloadFile(gifUrl, fileName, directoryPath);
        return directoryPath + fileName;
    }

    public String saveFileToUserFolder(String userId, String query, String filePathInDiskCache) throws IOException {
        String canonicalPath = new File(".").getCanonicalPath();

        File file = new File(canonicalPath + filePathInDiskCache);
        String toDirectoryPath = canonicalPath + usersDirectoryLocation + userId + "/" + query + "/";
        String toPath = toDirectoryPath + file.getName();

        File toDirectory = new File(toDirectoryPath);
        if (!toDirectory.exists()) {
            toDirectory.mkdir();
        }

        Path resultPath = Files.copy(
                Path.of(file.getCanonicalPath()),
                Path.of(toPath),
                StandardCopyOption.REPLACE_EXISTING);
        return resultPath.toString();
    }

    public List<String> getFilesLinksByQuery(String userId, String query) {
        List<String> gifPaths = null;
        try {
            String canonicalPath = new File(".").getCanonicalPath();
            String queryDirectoryPath = canonicalPath + usersDirectoryLocation + userId + "/" + query + "/";

            try (Stream<Path> paths = Files.walk(Paths.get(queryDirectoryPath))) {
                gifPaths = paths
                        .filter(Files::isRegularFile)
                        .map(Path::toString)
                        .collect(Collectors.toList());
            }

        } catch (IOException e) {
            //TODO create custom exception
            e.printStackTrace();
        }
        return gifPaths == null ? Collections.emptyList() : gifPaths;
    }

    public List<QueryResponseObject> getAllObjectsLinks(String userId) {
        List<QueryResponseObject> gifPaths = null;
        try {
            String canonicalPath = new File(".").getCanonicalPath();
            String queryDirectoryPath = canonicalPath + usersDirectoryLocation + userId + "/";

            try (Stream<Path> paths = Files.walk(Paths.get(queryDirectoryPath))) {
                gifPaths = paths
                        .filter(Files::isDirectory)
                        .filter(path -> !path.getFileName().toString().equals(userId))
                        .map(path -> new QueryResponseObject(
                                path.getFileName().toString(),
                                getFilesLinksByQuery(userId, path.getFileName().toString())
                        ))
                        .collect(Collectors.toList());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return gifPaths == null ? Collections.emptyList() : gifPaths;
    }

    public List<UserHistoryRecord> getUserHistory(String userId) throws IOException {
        return csvHelper.readUserHistoryFromFile(userId);
    }

    public void saveImageToInMemoryCache(String userId, String query, String savedFilePath) {
        if (inMemoryCache.get(userId) != null) {
            Map<String, Set<Path>> userMap = inMemoryCache.get(userId);
            if (userMap.containsKey(query)) {
                Set<Path> paths = CollectionUtils.extendSet(userMap.get(query), Path.of(savedFilePath));
                Map<String, Set<Path>> newUserMap =
                        CollectionUtils.extendMap(userMap, Map.of(query, paths));
                inMemoryCache.remove(userId);
                inMemoryCache.put(userId, newUserMap);
            }
        } else {
            inMemoryCache.put(userId, Map.of(query, Set.of(Path.of(savedFilePath))));
        }
    }

}
