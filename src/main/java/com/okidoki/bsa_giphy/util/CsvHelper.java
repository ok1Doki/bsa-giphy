package com.okidoki.bsa_giphy.util;

import com.okidoki.bsa_giphy.model.UserHistoryRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class CsvHelper {

    @Value("${app.users.location}")
    private String usersDirectoryLocation;

    public String convertToCSV(String[] data) {
        return Stream.of(data)
                .map(word -> escapeSpecialCharacters(word))
                .collect(Collectors.joining(","));
    }

    public String escapeSpecialCharacters(String data) {
        String escapedData = data.replaceAll("\\R", " ");
        if (data.contains(",") || data.contains("\"") || data.contains("'")) {
            data = data.replace("\"", "\"\"");
            escapedData = "\"" + data + "\"";
        }
        return escapedData;
    }

    public void writeUserGeneratingHistory(UserHistoryRecord record, String userId) throws IOException {
        String csvFilePath =
                new File(".").getCanonicalPath() + usersDirectoryLocation + userId + "/history.csv";

        File historyFile = new File(csvFilePath);

        try (FileWriter pw = new FileWriter(historyFile, true)) {
            pw.append(convertToCSV(new String[]{record.getDate(), record.getQuery(), record.getGif()}));
            pw.append("\r\n");
        }
    }

    public List<UserHistoryRecord> readUserHistoryFromFile(String userId) throws IOException {
        File file = new File(new File(".").getCanonicalPath() + usersDirectoryLocation + userId + "/history.csv");

        List<UserHistoryRecord> records = new ArrayList<>();
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                records.add(getRecordFromLine(scanner.nextLine()));
            }
        }
        return records;
    }

    private UserHistoryRecord getRecordFromLine(String line) {
        UserHistoryRecord record = new UserHistoryRecord();
        try (Scanner rowScanner = new Scanner(line)) {
            rowScanner.useDelimiter(",");
            while (rowScanner.hasNext()) {
                record.setDate(rowScanner.next());
                record.setQuery(rowScanner.next());
                record.setGif(rowScanner.next());
            }
        }
        return record;
    }

    public void clearFile(String userId) throws IOException {
        File file = new File(new File(".").getCanonicalPath() +
                usersDirectoryLocation +
                userId +
                "/history.csv");
        file.delete();
    }
}
