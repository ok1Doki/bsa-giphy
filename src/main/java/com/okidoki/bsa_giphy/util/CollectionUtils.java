package com.okidoki.bsa_giphy.util;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;

import java.util.Map;
import java.util.Set;

public class CollectionUtils {

    public static <T, U> Map<T, U> extendMap(Map<T, U> original, Map<T, U> changes) {
        return ImmutableMap.<T, U>builder()
                .putAll(changes)
                .putAll(Maps.filterKeys(original, key -> !changes.containsKey(key)))
                .build();
    }

    public static <T> ImmutableSet<T> extendSet(Set<T> original, T element) {
        return new ImmutableSet.Builder<T>()
                .addAll(original)
                .add(element)
                .build();
    }
}
