package com.okidoki.bsa_giphy.util;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;

public class FileHelper {

    public static void downloadFile(String url, String fileName, String filePath) throws IOException {
        File directory = new File(filePath);
        if (!directory.exists()) {
            directory.mkdirs();
        }

        byte[] b = new byte[1];
        URLConnection urlConnection = new URL(url).openConnection();
        urlConnection.connect();

        try (DataInputStream di = new DataInputStream(urlConnection.getInputStream());
             FileOutputStream fo = new FileOutputStream(filePath + "/" + fileName)
        ) {
            while (-1 != di.read(b, 0, 1))
                fo.write(b, 0, 1);
        }
    }
}
