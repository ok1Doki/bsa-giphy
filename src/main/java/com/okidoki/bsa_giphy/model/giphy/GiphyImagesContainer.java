package com.okidoki.bsa_giphy.model.giphy;

import lombok.Data;

@Data
public class GiphyImagesContainer {

    private GiphyImage fixed_height;
    private GiphyImage fixedHeightStill;
    private GiphyImage fixedHeightDownsampled;
    private GiphyImage fixedWidth;
    private GiphyImage fixedWidthStill;
    private GiphyImage fixedWidthDownsampled;
    private GiphyImage fixedHeightSmall;
    private GiphyImage fixedHeightSmallStill;
    private GiphyImage fixedWidthSmall;
    private GiphyImage fixedWidthSmallStill;
    private GiphyImage downsized;
    private GiphyImage downsizedStill;
    private GiphyImage downsizedMedium;
    private GiphyImage downsizedLarge;
    private GiphyOriginal original;
    private GiphyImage original_still;
    private GiphyImage looping;

}
