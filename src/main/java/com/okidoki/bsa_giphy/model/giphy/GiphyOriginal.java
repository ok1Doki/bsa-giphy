package com.okidoki.bsa_giphy.model.giphy;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class GiphyOriginal extends GiphyImage{
    private String frames;
}
