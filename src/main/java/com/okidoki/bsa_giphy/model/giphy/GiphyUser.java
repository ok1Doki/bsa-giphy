package com.okidoki.bsa_giphy.model.giphy;

import lombok.Data;

@Data
public class GiphyUser {
    private String avatarUrl;
    private String bannerUrl;
    private String profileUrl;
    private String username;
    private String display_name;
}
