package com.okidoki.bsa_giphy.model.giphy;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
public class GiphyImage {
    private String url;
    private String width;
    private String height;
    private String size;
    private String mp4;
    private String mp4Size;
    private String webp;
    private String webpSize;
}