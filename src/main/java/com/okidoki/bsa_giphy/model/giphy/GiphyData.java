package com.okidoki.bsa_giphy.model.giphy;

import lombok.Data;

@Data
public class GiphyData {

    private String type = "gif";
    private String id;
    private String slug;
    private String url;
    private String bitly_url;
    private String embed_url;
    private String username;
    private String source;
    private String rating;
    private String contentUrl;
    private GiphyUser user;
    private String sourceTld;
    private String sourcePostUrl;
    private String updateDatetime;
    private String createDatetime;
    private String importDatetime;
    private String trendingDatetime;
    private GiphyImagesContainer images;
    private String title;


}
