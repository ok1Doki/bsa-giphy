package com.okidoki.bsa_giphy.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserHistoryRecord {
    private String date;
    private String query;
    private String gif;
}
