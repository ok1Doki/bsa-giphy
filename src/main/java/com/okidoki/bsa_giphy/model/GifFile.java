package com.okidoki.bsa_giphy.model;

import com.okidoki.bsa_giphy.model.giphy.GiphyData;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GifFile {
    private GiphyData giphyData;
    private String url;
    private String fileName;
}
