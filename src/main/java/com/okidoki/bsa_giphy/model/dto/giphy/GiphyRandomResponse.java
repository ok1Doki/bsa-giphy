package com.okidoki.bsa_giphy.model.dto.giphy;

import com.okidoki.bsa_giphy.model.giphy.GiphyData;
import com.okidoki.bsa_giphy.model.dto.giphy.common.Meta;
import lombok.Data;

@Data
public class GiphyRandomResponse {

    private GiphyData data;
    private Meta meta;

}
