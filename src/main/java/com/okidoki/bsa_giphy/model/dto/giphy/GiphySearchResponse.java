package com.okidoki.bsa_giphy.model.dto.giphy;

import com.okidoki.bsa_giphy.model.giphy.GiphyData;
import com.okidoki.bsa_giphy.model.dto.giphy.common.Meta;
import com.okidoki.bsa_giphy.model.dto.giphy.common.Pagination;
import lombok.Data;

import java.util.List;

@Data
public class GiphySearchResponse {

    private List<GiphyData> data;
    private Pagination pagination;
    private Meta meta;

}
