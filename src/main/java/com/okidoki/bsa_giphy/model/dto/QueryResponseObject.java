package com.okidoki.bsa_giphy.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QueryResponseObject {
    private String query;
    private List<String> gifs;
}
