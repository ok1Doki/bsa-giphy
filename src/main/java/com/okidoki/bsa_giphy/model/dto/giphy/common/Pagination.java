package com.okidoki.bsa_giphy.model.dto.giphy.common;

import lombok.Data;

@Data
public class Pagination {

    private int totalCount;
    private int count;
    private int offset;

}