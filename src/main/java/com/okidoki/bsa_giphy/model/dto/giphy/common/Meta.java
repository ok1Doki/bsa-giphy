package com.okidoki.bsa_giphy.model.dto.giphy.common;

import lombok.Data;

@Data
public class Meta {

    private int status;
    private String msg;

}